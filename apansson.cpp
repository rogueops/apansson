#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>

#include "astar.h"

struct TrackMetadata {
  std::string track;
  std::string album;
  std::string artist;
  int year;

  bool operator==(const TrackMetadata &rhs) const {
    return track == rhs.track &&
        album == rhs.album &&
        artist == rhs.artist &&
        year == rhs.year;
  }

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & track;
    ar & album;
    ar & artist;
    ar & year;
  }

};

struct AlbumMetadata {
  std::string album;
  std::string artist;
  int year;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & album;
    ar & artist;
    ar & year;
  }

};

struct MetadataLookup {
  std::string uri;
  TrackMetadata metadata;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & uri;
    ar & metadata;
  }

};

struct StoredState {
  int width;
  int height;
  int turn_limit;
  int favorite_decade;
  std::vector<TrackMetadata> top_tracks;
  std::vector<AlbumMetadata> top_albums;
  std::vector<std::string> top_artists;
  std::vector<std::string> hated_artists;
  std::vector<MetadataLookup> lookups;

  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & width;
    ar & height;
    ar & turn_limit;
    ar & favorite_decade;
    ar & top_tracks;
    ar & top_albums;
    ar & top_artists;
    ar & hated_artists;
    ar & lookups;
  }

};

using Level = std::vector<std::vector<std::string>>;

struct TurnState {
  int turn_number;
  int remaining_capacity;
  int remaining_time;
  int boost_cooldown;
  Level level;
};

struct Node {
  Node(int xx, int yy) : x(xx), y(yy) {}
  Node() = default;
  bool operator==(const Node &rhs) const {
    return x == rhs.x && y == rhs.y;
  }
  // Needed for std::set
  bool operator<(const Node &rhs) const {
    if (y < rhs.y) {
      return true;
    } else if (y == rhs.y) {
      if (x < rhs.x)
        return true;
    }
    return false;
  }
  int x = 0;
  int y = 0;
};

std::vector<std::string> split(const std::string &s, char delim) {
  std::stringstream ss(s);
  std::string item;
  std::vector<std::string> elems;
  while (std::getline(ss, item, delim)) {
    elems.push_back(item);
  }
  return elems;
}

bool parse_init(std::istream &s, StoredState &is) {
  s >> is.width;
  s >> is.height;
  s >> is.turn_limit;
  int n{};
  s >> n;
  s.ignore(1000, '\n');
  std::map<int, int> decades;
  for (int i = 0; i < n; ++i) {
    std::string line;
    std::getline(s, line);
    std::vector<std::string> data = split(line, ',');
    int year = std::atoi(data[3].c_str());
    decades[year/10] += 1;
    TrackMetadata tm{data[0], data[1], data[2], year};
    is.top_tracks.push_back(tm);
  }
  s >> n;
  s.ignore(1000, '\n');
  for (int i = 0; i < n; ++i) {
    std::string line;
    std::getline(s, line);
    std::vector<std::string> data = split(line, ',');
    int year = std::atoi(data[2].c_str());
    decades[year/10] += 1;
    AlbumMetadata am{data[0], data[1], year};
    is.top_albums.push_back(am);
  }
  int best_decade = 0;
  int best_decade_count = 0;
  for (auto it: decades) {
    if (it.second > best_decade_count)
      best_decade = it.first;
  }
  //  std::cerr << "Best decade: " << best_decade * 10 << std::endl;
  is.favorite_decade = best_decade;
  s >> n;
  s.ignore(1000, '\n');
  for (int i = 0; i < n; ++i) {
    std::string line;
    std::getline(s, line);
    is.top_artists.push_back(line);
  }
  s >> n;
  s.ignore(1000, '\n');
  for (int i = 0; i < n; ++i) {
    std::string line;
    std::getline(s, line);
    is.hated_artists.push_back(line);
  }
  return true;
}

bool parse_turn(std::istream &s, StoredState &is, TurnState &ts) {
  s >> ts.turn_number;
  s >> ts.remaining_capacity;
  s >> ts.remaining_time;
  s >> ts.boost_cooldown;
  int n{};
  s >> n;
  s.ignore(1000, '\n');
  for (int i = 0; i < n; ++i) {
    std::string line;
    std::getline(s, line);
    std::vector<std::string> data = split(line, ',');
    std::string uri(data[0]);
    // Weird, is the spec wrong?
    TrackMetadata tm{data[1], data[3], data[2], std::atoi(data[4].c_str())};
    is.lookups.push_back({uri, tm});
  }
  for (int i = 0; i < is.height; ++i) {
    std::string line;
    std::getline(s, line);
    std::vector<std::string> data = split(line, ',');
    ts.level.push_back(data);
  }
  return true;
}

void save_state(const std::string &mid, StoredState &is) {
  std::stringstream ss;
  ss << "apansson_";
  ss << mid;
  std::ofstream ofs(ss.str());
  boost::archive::text_oarchive oa(ofs);
  oa << is;
}

void load_state(const std::string &mid, StoredState &is) {
  std::stringstream ss;
  ss << "apansson_";
  ss << mid;
  std::ifstream ifs(ss.str());
  boost::archive::text_iarchive ia(ifs);
  ia >> is;
}

int get_score(const MetadataLookup &lookup, const StoredState &is) {
  for (std::string artist: is.hated_artists) {
    if (lookup.metadata.artist == artist)
      return -16;
  }
  for (TrackMetadata tm: is.top_tracks) {
    if (lookup.metadata == tm)
      return -4;
  }
  // std::cerr << "uri: " << lookup.uri << std::endl;
  // std::cerr << "artist: " << lookup.metadata.artist << std::endl;
  // std::cerr << "album: " << lookup.metadata.album << std::endl;
  int tier = 0;
  for (std::string artist: is.top_artists) {
    // std::cerr << "top artist: " << artist << std::endl;
    if (lookup.metadata.artist == artist) {
      tier += 1;
      break;
    }
  }
  for (AlbumMetadata am: is.top_albums) {
    // std::cerr << "top album: " << am.album << std::endl;
    if (lookup.metadata.album == am.album) {
      tier += 1;
      break;
    }
  }
  // std::cerr << lookup.metadata.year / 10 << " " << is.favorite_decade << std::endl;
  if (lookup.metadata.year / 10 == is.favorite_decade) {
    tier += 1;
  }

  // std::cerr << "Tier: " << tier << std::endl;

  switch (tier) {
    case 1:
      return 4;
    case 2:
      return 16;
    case 3:
      return 64;
    default:
      break;
  }
  return 0;
}


std::function<double(Node, Node)> gf(const StoredState &is, const TurnState &ts) {
  std::string track_uri_prefix("spotify:track:");
  auto g = [=] (Node n1, Node n2) {
    const std::string &cell = ts.level[n2.y][n2.x];
    if (cell == "U") {
      return 1000.0;
    } else if (cell.compare(0, track_uri_prefix.length(), track_uri_prefix) == 0) {
      int score = -1;
      for (auto lookup: is.lookups) {
        if (lookup.uri == cell) {
          score = get_score(lookup, is);
          break;
        }
      }
      if (score < 0)
        return 5.0;
    }

    return 1.0;
  };
  return g;
}

std::function<std::vector<Node>(Node)> nf(const StoredState &is, const TurnState &ts) {
  auto neighbours = [=] (Node n) {
    std::vector<Node> nb;
    std::vector<Node> delta{{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
    std::string wall("#");
    std::string track_uri_prefix("spotify:track:");
    const Level &w = ts.level;
    for (auto d: delta) {
      Node nn{n.x + d.x, n.y + d.y};
      if (0 <= nn.y && nn.y < w.size() &&
          0 <= nn.x && nn.x < w[0].size()) {
        const std::string &cell = w[nn.y][nn.x];
        if (cell != wall && cell[0] != 'M') {
          if (ts.remaining_capacity <= 0) {
            if (cell.compare(0, track_uri_prefix.length(), track_uri_prefix) != 0) {
              nb.push_back(nn);
            }
          } else {
            nb.push_back(nn);
          }
        }
      }
    }
    return nb;
  };
  return neighbours;
}

std::function<double(Node)> hf(Node goal) {
  auto h = [=] (Node n) -> double {
    return std::abs(n.x - goal.x) + std::abs(n.y - goal.y);
  };
  return h;
}

std::tuple<double, std::vector<Node>> find_path(Node start, Node goal, const StoredState &is, const TurnState &ts) {
  double f;
  std::vector<Node> p;
  auto g = gf(is, ts);
  auto h = hf(goal);
  auto n = nf(is, ts);
  return apansson::astar(start,
                         goal,
                         g,
                         h,
                         n);
}

std::string get_move(Node a, Node b) {
  //  std::cerr << a.x << ", " << a.y << " -> " << b.x << ", " << b.y << std::endl;
  if (a.x > b.x)
    return "W";
  else if (a.x < b.x)
    return "E";
  else if (a.y < b.y)
    return "S";
  else
    return "N";
}

std::string calc_move(const std::string &mid, const StoredState &is, const TurnState &ts) {
  Node pos;
  Node user_pos;
  //  std::cerr << "Time: " << ts.remaining_time << std::endl;
  std::vector<std::pair<Node, std::string>> track_uris;
  std::string track_uri_prefix("spotify:track:");
  for (int y = 0; y < is.height; ++y) {
    for (int x = 0; x < is.width; ++x) {
      const std::string &cell = ts.level[y][x];
      if (cell == mid) {
        pos = {x, y};
      } else if (cell.compare(0, track_uri_prefix.length(), track_uri_prefix) == 0) {
        track_uris.push_back({{x, y}, cell});
      } else if (cell == "U") {
        user_pos = {x, y};
      }
    }
  }

  double user_f;
  std::vector<Node> user_path;
  std::tie(user_f, user_path) = find_path(pos, user_pos, is, ts);
  //  std::cerr << ts.turn_number << " " << user_path.size() << " " << is.turn_limit << std::endl;
  if (ts.turn_number + user_path.size() + 3 >= is.turn_limit) {
    std::string move = get_move(pos, user_path[user_path.size() - 2]);
    //    std::cerr << "Run home move: " << move << std::endl;
    return move;
  }

  // for (auto ti: track_uris) {
  //   Node n;
  //   std::string uri;
  //   std::tie(n, uri) = ti;
  //   std::cout << n.x << ", " << n.y << ": " << uri << std::endl;
  // }
  double f;
  std::vector<Node> path;
  using Path = std::tuple<double, std::string, std::vector<Node>>;
  std::vector<Path> paths;
  for (auto uri: track_uris) {
    auto res = find_path(pos, uri.first, is, ts);
    paths.push_back(std::make_tuple(std::get<0>(res), uri.second, std::get<1>(res)));
  }
  auto comp = [] (const Path &p1, const Path &p2) -> bool {
    return std::get<0>(p1) < std::get<0>(p2);
  };
  std::sort(paths.begin(), paths.end(), comp);

  std::vector<Path> browsed;
  std::vector<MetadataLookup> browsed_metadata;
  std::vector<Path> to_browse;
  if (ts.remaining_capacity > 0 && !paths.empty()) {
    for (auto p: paths) {
      std::string uri = std::get<1>(p);
      bool already_browsed = false;
      for (MetadataLookup lookup: is.lookups) {
        if (lookup.uri == uri) {
          browsed_metadata.push_back(lookup);
          already_browsed = true;
          break;
        }
      }
      if (already_browsed) {
        browsed.push_back(p);
      } else {
        to_browse.push_back(p);
      }
    }
    int browse_limit = std::min<int>(10, paths.size());
    //    std::cerr << "Browsed: " << browsed.size() << std::endl;
    //std::cerr << "To browse: " << to_browse.size() << std::endl;
    if (browsed.size() >= browse_limit) {
      std::vector<int> thresholds{64, 16, 4, 0};
      for (int j = 0; j < thresholds.size() && path.empty(); ++j) {
        int threshold = thresholds[j];
        std::cerr << "Threshold: " << threshold << std::endl;
        for (int i = 0; i < browsed.size(); ++i) {
          int score = get_score(browsed_metadata[i], is);
          if (score >= threshold) {
            const std::vector<Node> &p = std::get<2>(browsed[i]);
            std::tie(f, path) = find_path(pos, p[0], is, ts);
            break;
          } else {
            //std::cerr << "score: " << score << std::endl;
          }
        }
      }
    }

    std::cerr << "path: " << path.size() << " " << to_browse.size() << std::endl;
    if (path.empty() && !to_browse.empty()) {
      std::string command = "B,";
      for (int i = 0; i < to_browse.size() && i < 3; ++i) {
        // TODO: boost
        const std::string &uri = std::get<1>(to_browse[i]);
        std::cerr << "Browse: " << uri << " " << ts.boost_cooldown << std::endl;
        if (ts.boost_cooldown == 0) {
          command += uri;
          command += ",";
        } else {
          return uri;
        }
      }
      command = command.substr(0, command.size() - 1);
      std::cerr << command << std::endl;
      return command;
    }
  }

  if (path.empty()) {
    path = user_path;
  }

  if (ts.boost_cooldown == 0 && path.size() > 3) {
    std::string command = "B,";
    for (int i = path.size() - 2; i > 0 && i > path.size() - 5; --i) {
      command += get_move(path[i + 1], path[i]);
      command += ",";
    }
    command = command.substr(0, command.size() - 1);
    return command;
  } else {
    return get_move(pos, path[path.size() - 2]);
  }

  std::cerr << "Aagh, no move?!" << std::endl;
}

int main(int argc, char *argv[]) {
  std::cerr << "apansson: ";
  if (!std::cin.good())
    std::cerr << "cin not good" << std::endl;
  if (std::cin.eof())
    std::cerr << "cin eof" << std::endl;
  std::string turn_type;
  std::cin >> turn_type;
  std::string mid;
  std::cin >> mid;
  std::cerr << "Turn type: \"" << turn_type << "\" id: \"" << mid << "\"" << std::endl;
  StoredState is;
  TurnState ts;
  if (turn_type == "INIT") {
    parse_init(std::cin, is);
    save_state(mid, is);
  } else if (turn_type == "TURN") {
    load_state(mid, is);
    parse_turn(std::cin, is, ts);
    std::string move = calc_move(mid, is, ts);
    std::cout << move << std::endl;
    std::cerr << "Move: \"" << move << "\"" << std::endl;
    save_state(mid, is);
  } else {
    // Panic!
    std::cout << "S";
  }
  return 0;
}
