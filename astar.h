#include <functional>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>

namespace apansson {

template <typename T>
void reconstruct_path(const std::map<T, T> &came_from, T current_node, std::vector<T> &path) {
  auto it = came_from.find(current_node);
  path.push_back(current_node);
  if (it != came_from.end()) {
    reconstruct_path(came_from, it->second, path);
  }
}

template <typename S, typename T>
std::tuple<S, std::vector<T>> astar(
    T start,
    T goal,
    std::function<S(T, T)> g_fcn,
    std::function<S(T)> h_fcn,
    std::function<std::vector<T>(T)> find_neighbours_fcn) {
  std::set<T> closed_list;
  using pqelem = std::pair<S, T>;
  auto comp = [] (const pqelem &a, const pqelem &b) -> bool { return a.first > b.first; };
  std::priority_queue<pqelem,
                      std::vector<pqelem>,
                      decltype(comp)> open_list(comp);
  std::set<T> open_list_nodes;
  open_list.push({h_fcn(start), start});
  open_list_nodes.insert(start);
  std::map<T, S> g_score{{start, S{}}};
  std::map<T, T> came_from;
  while (!open_list.empty()) {
    S f;
    T node;
    std::tie(f, node) = open_list.top();
    open_list.pop();
    open_list_nodes.erase(node);
    if (node == goal) {
      std::vector<T> path;
      reconstruct_path(came_from, goal, path);
      return std::make_tuple(f, path);
    }
    closed_list.insert(node);
    std::vector<T> neighbours = find_neighbours_fcn(node);
    for (auto neighbour: neighbours) {
      S new_g = g_score[node] + g_fcn(node, neighbour);

      auto cit = closed_list.find(neighbour);
      if (cit != closed_list.end() && new_g > g_score[neighbour]) {
        continue;
      }

      bool new_is_better = false;
      auto oit = open_list_nodes.find(neighbour);
      if (oit == open_list_nodes.end()) {
        f = new_g + h_fcn(neighbour);
        open_list.push({f, neighbour});
        open_list_nodes.insert(neighbour);
        new_is_better = true;
      } else if (new_g < g_score[neighbour]) {
        new_is_better = true;
      } else {
        new_is_better = false;
      }

      if (new_is_better) {
        came_from[neighbour] = node;
        g_score[neighbour] = new_g;
      }
    }
  }

  return std::make_tuple(S{}, std::vector<T>());
}

}  // namespace apansson
