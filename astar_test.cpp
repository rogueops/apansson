#include "astar.h"

#include <cmath>
#include <string>

struct Node {
  Node(int xx, int yy) : x(xx), y(yy) {}
  Node() = default;
  bool operator==(const Node &rhs) const {
    return x == rhs.x && y == rhs.y;
  }
  // Needed for std::set
  bool operator<(const Node &rhs) const {
    if (y < rhs.y) {
      return true;
    } else if (y == rhs.y) {
      if (x < rhs.x)
        return true;
    }
    return false;
  }
  int x = 0;
  int y = 0;
};

using World = std::vector<std::string>;

double g(Node n1, Node n2) {
  return 1.0;
}

std::function<std::vector<Node>(Node)> nf(const World &w) {
  auto neighbours = [=] (Node n) {
    std::vector<Node> nb;
    std::vector<Node> delta{{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
    for (auto d: delta) {
      Node nn{n.x + d.x, n.y + d.y};
      if (0 <= nn.y && nn.y < w.size() &&
          0 <= nn.x && nn.x < w[0].size()) {
        if (w[nn.y][nn.x] != '*') {
          nb.push_back(nn);
        }
      }
    }
    return nb;
  };
  return neighbours;
}

std::function<double(Node)> hf(Node goal) {
  auto h = [=] (Node n) -> double {
    return std::abs(n.x - goal.x) + std::abs(n.y - goal.y);
  };
  return h;
}

Node find(const World &w, char sym) {
  for (int y = 0; y < w.size(); ++y) {
    for (int x = 0; x < w[y].size(); ++x) {
      if (w[y][x] == sym)
        return {x, y};
    }
  }
  return {0, 0};
}

void testworld(World &world) {
  Node start = find(world, 's');
  Node goal = find(world, 'g');
  double f;
  std::vector<Node> p;
  auto h = hf(goal);
  auto n = nf(world);
  std::tie(f, p) = apansson::astar(start,
                                   goal,
                                   std::function<double(Node, Node)>(g),
                                   h,
                                   n);
  for (Node n: p) {
    if (n == start)
      continue;
    if (n == goal)
      continue;
    world[n.y][n.x] = '+';
  }
  for (auto row: world) {
    std::cout << row << std::endl;
  }
}

int main(int, char*[]) {
  World w1{"**************************",
        "*                        *",
        "*                     s  *",
        "*                        *",
        "*     ********************",
        "*                        *",
        "*                        *",
        "***********   ************",
        "* g                      *",
        "*                        *",
        "**************************"};
  World w2{"**************************",
        "*                 s      *",
        "*                        *",
        "*                        *",
        "*     ********************",
        "*                        *",
        "*                        *",
        "***********   ************",
        "* g                      *",
        "*                        *",
        "**************************"};
  World w3{"**************************",
        "* s                      *",
        "*                        *",
        "*                        *",
        "*     ********************",
        "*                        *",
        "*                        *",
        "***********   ************",
        "*                        *",
        "*                   g    *",
        "**************************"};
  testworld(w1);
  testworld(w2);
  testworld(w3);
}
